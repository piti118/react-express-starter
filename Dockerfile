FROM node:5.11.0

WORKDIR /myapp

COPY app /myapp

RUN npm install

EXPOSE 3000


CMD ./run.sh

var configs = {}

configs['development'] = {
  knex_config: {
    client: 'pg',
    connection: {
      host     : 'localhost',
      database : 'abc'
    }
  }
}

configs['production'] = {
  knex_config:  {
    client: 'pg',
    connection: {
      host     : 'database',
      user     : 'user',
      password : 'password',
      database : 'cde'
    }
  }
}

env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development'

config = configs[env]

module.exports = config

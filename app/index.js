'use strict'

var express = require("express");
var app     = express();
var path    = require("path");
var Chance = require('chance');

var config = require('./config.js')
var knex = require('knex')(config.knex_config)

var chance = new Chance()

app.use('/build', express.static('build'));
app.use('/examples', express.static('examples'));

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodie

app.get('/', function(req, res){
  let target = path.join(__dirname, "/index.html")
  res.sendFile(target)
})

app.get('/listname', function(req, res){
  knex.select('name').from('users')
    .then((data)=>{
      res.json(data)
    })
})

app.post('/addname', (req, res)=>{
  console.log(req.body.name)
  res.json("OK")
})

app.get('/data', function(req, res){
  let a = {
    name: "hello",
    lastname: "yes",
    arr: [1,2,3,4,5]
  }
  res.json(a)
})

app.get('/randomname', (req, res)=>{
  res.json({name: chance.name()})
})

app.get('/slowname', (req,res)=>{
  setTimeout( ()=>res.json({name: chance.name()}), 2000 )
})


app.get('/data2', function(req, res){
  let a = {
    name: "hello",
    lastname: "yes",
    arr: [1,2,3,4,5]
  }
  res.json(a)
})

// app.get('/',function(req,res){
//   res.sendFile(path.join(__dirname+'/index.html'));
//   //__dirname : It will resolve to your project folder.
// });
//
// app.get('/about',function(req,res){
//   res.sendFile(path.join(__dirname+'/about.html'));
// });
//
// app.get('/sitemap',function(req,res){
//   res.sendFile(path.join(__dirname+'/sitemap.html'));
// });

app.listen(3000);

console.log("Running at Port 3000");

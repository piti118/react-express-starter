var config = require('./config.js')
var knex = require('knex')(config.knex_config)

knex.schema.dropTableIfExists('users').then(()=>{

  return knex.schema.createTableIfNotExists('users', function (table) {
    table.increments();
    table.string('name');
    table.timestamps();
  })
  .then(()=>{
    var jobs = [1,2,3,4,5].map((x) => knex('users').insert({name: 'name'+x}))
    return Promise.all(jobs)
  })
  .then(()=>{
    return knex.destroy()
  })
})
